# Building CI images

This sub-project generates and pushes to gitlab.com
docker registry the docker images to be used for compiling the
master branch of the library.

The reason for pre-generating the images is to speed-up CI runs
and avoid failures due to downloading of individual packages (e.g.,
because some mirrors were down).


# How to generate a new image

Add a new directory with a Dockerfile containing the instructions
for the image.

Then edit .gitlab-ci.yml to add the build instructions, commit and push.


# How to re-generate an existing image

Visit the [Pipeline page](https://gitlab.com/rockdaboot/libhsts/pipelines) and click
on 'Run Pipeline'.

The image will be re-build.


# Building locally (example for buildenv-debian-sid)
docker login registry.gitlab.com
docker build -t registry.gitlab.com/rockdaboot/build-images:buildenv-debian-sid docker-debian-sid
docker push registry.gitlab.com/rockdaboot/build-images:buildenv-debian-sid


# Build and test outside Gitlab registry
$ docker build docker-debian-sid
...
Successfully built 54b10afffc6c
$ docker run -it 54b10afffc6c /bin/bash
# git clone https://gitlab.com/rockdaboot/libhsts.git
# cd libhsts
...
