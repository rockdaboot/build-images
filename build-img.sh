#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

if test "$CI_PROJECT_NAMESPACE" = "rockdaboot" && test "$CI_PROJECT_NAME" = "build-images";then
	set -e
	docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $registry
	docker build -t $registry/$project:$image $startdir
	docker push $registry/$project:$image
fi

exit 0
